# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [3.1.3] - 2019-07-21

### Added

- Dokumentation bekannter Probleme und Lösungen

- documentation of known problems and solutions

### Fixed

- Optionenkollision für ulem und truncate

- option clash for ulem and truncate


## [3.1.2] - 2019-01-26

### Fixed

- Probleme mit `amsart`: Ausgabe von `\section*` und nicht definiertes `\@dotfill`

- problems with `amsart`: output of `\section*` and undefined `\@dotfill`


## [3.1.1] - 2018-12-18

### Fixed

- schlecht austarierte Füllpunkte in summaries

- uneven dot fills in summaries


## [3.1.0] - 2018-12-17

### Added

- neues Skript `pyMergeChanges.py` für die Entfernung von Markierungen

- new script `pyMergeChanges.py` for removing markup

### Changed

- Handbuch überarbeitet und aufgehübscht

- updated and refrehed user manual

### Removed

- altes Skript `delcmdchanges.bash`, das nicht mehr funktionierte

- old script `delcmdchanges.bash` which does not work anymore


## [3.0.0] - 2018-11-04

### Added

- Befehl `comment` und zugehörige Hilfsbefehle/-zähler
- Befehl `highlight` und zugehörige Hilfsbefehle/-zähler
- Änderungslistenstil `compactsummary`
- Titel-Option für Änderungslisten `title`
- `show`-Option für Änderungslisten (Einschränkung der Anzeige auf bestimmte Typen von Änderungen)
- Befehle `setsummarywidth` und `setsummarytowidthof` zum Setzen der Breite der Änderungszusammenfassung
- Befehl `settruncatewidth` zum Setzen der Breite der Textkürzung in der Änderungsliste

- command `comment` and the belonging helper commands resp. counters
- command `highlight` and the belonging helper commands resp. counters
- style for list of changes `compactsummary`
- `title` option for list of changes
- `show` option for list of changes, specify types of changes to show
- commands `setsummarywidth` and `setsummarytowidth` for setting the width of the summary
- command `settruncatewidth` for setting width of truncation in list of changes

### Changed

- default Markup für zugefügten und gelöschten Text umbenannt von `none` zu `colored`
- Anmerkungen werden als Kommentare formatiert
- Änderungsliste: etwas breitere Kürzung des Textes und deutlichere Kürzungsmarkierung
- Standardfarbe für Autoren ist blau (vorher schwarz)

- default markup for added and deleted text renamed from `none` to `colored`
- remarks are formatted as comments
- list of changes: slightly wider truncation of text, clearer truncation marker
- default color for authors is blue (from black)

### Deprecated

- optionaler Parameter `remark`, ersetzt durch `comment` in 3.0.0, wird in Version 4 entfernt werden; Befehl `\setremarkmarkup` ist bereits entfernt

- optional parameter `remark`, replaced by `comment` in 3.0.0, will be removed in Version 4; command `\setremarkmarkup` is removed yet

### Removed

- `decision` Keys für Änderungskommandos (waren nie voll implementiert und sind zu kompliziert für den Zweck des Pakets)
- Anmerkungs-Markup-Befehl `\setremarkmarkup`
- Markup `none`, `sout` und `xout` für zugefügten Text
- `textsubscript`-Befehl, der mittlerweile Standard-LaTeX ist

- `decision` keys for markup commands (they were not fully implemented yet and are much to complicated for the purpose of the package)
- remark markup command `\setremarkmarkup`
- markup `none`, `sout`, and `xout` for added text
- command `textsubscript`, which is standard LaTeX now

### Fixed

- Rechtschreibfehler in readme und Release Notes
- Leerzeichenprobleme im Code
- Zeilenumbruch zwischen alten und neuen Text bei `replaced` jetzt erlaubt
- zu viele Klammern bei `authormarkup=brackets` (galt für alle Autormarkierungen)
- Fehler bei der Ausgabe des Autorennamens bei Autoren ohne Name
- Option `nocolor` erzeugt Fehler beim nächsten LaTeX-Lauf mit Farben

- typo in description of readme and release notes
- space issues in the code
- linebreak between old and new text allowed for `replaced`
- too much brackets with `authormarkup=brackets` (was wrong for all author markups)
- error with author name output for authors without name
- option `nocolor` broke next LaTeX run with colors


## [2.1.0] - 2018-10-10

### Added

- readme
- Lizenzdatei
- Seitenumbrüche für Kapitel in Nutzerhandbuch
- changelog, dafür Versionsinfo im Handbuch entfernt
- Warnung bei Angabe eines falschen Stils für die Änderungsliste (Issue #5 bei gitlab)

- readme
- license file
- page breaks for chapters in user manual
- changelog, removed version information in user manual
- warning when using a wrong style for list of changes (issue #5 at gitlab)

### Removed

- Versionsinfo im Handbuch entfernt, ersetzt durch changelog

- removed version information in user manual, replaced by changelog

### Fixed

- Dokumentationsfehler für geänderten Text (Issue #19 bei sourceforge)
- mehrfache Leerzeichen bei allen Auszeichnungen mit `final`-Option (Issue #2 bei gitlab)
- mehrfache blanks vor Änderungsliste (Issue #3 bei gitlab)
- Überschrift der Änderungsliste bei `final`-Option (Issue #4 bei gitlab)

- documentation bug for replaced text (issue #19 at sourceforge)
- additional spaces with all change commands and option `final` (issue #2 at gitlab)
- additional blanks before list of changes (issue #3 at gitlab)
- heading of list of changes with option `final` (issue #4 at gitlab)


## [2.0.4] - 2015-04-27

### Fixed

- Fehler bei Benutzung einer unbekannten Sprache

- error when using an unknown language


## [2.0.3] - 2014-10-15

### Fixed

- Crash bei Einsatz der `amsart`-Klasse

- crash when used with the `amsart` class


## [2.0.2] - 2013-08-13

### Changed

- kleine Erweiterungen der Doku

- improved documentation

### Fixed

- Probleme mit korrupten Dokumentations-PDFs bei CTAN gefixt
- Doku

- fixed problem of corrupt documentation PDFs in CTAN
- documentation


## [2.0.1] - 2013-08-10

### Changed

- alle nötigen Dateien in CTAN-Archiv gepackt

- put all needed files in CTAN archive

### Fixed

- Upload-Probleme bei CTAN gefixt (falsche Zeilenenden)

- fixed upload problems with CTAN (wrong line endings)


## [2.0.0] - 2013-06-30

### Added

- "richtige" Liste der Änderungen, alte Zusammenfassung jetzt über den optionalen Parameter `style=summary`
- neues Autormarkup `none`
- Scriptbeschreibung um Parameter `-i` ergänzt

- "real" list of changes, old summary now with optional parameter `style=summary`
- new author markup `none`
- completed script description with `-i` parameter

### Changed

- `setlocextension` umbenannt in `setsocextension`

- renamed `setlocextension` to `setsocextension`

### Fixed

- Problem mit einigen Sonderzeichen in der Änderungszusammenfassung gelöst

- fixed problem with special characters in summary of changes


## [1.0.0] - 2012-04-25

### Added

- Key-Value-Interface für Änderungsmanagement-Kommandos
- Fehlermeldung bei Verwendung einer ungültigen Autor\_innen-ID
- Leerzeichen vor Autor\_innenname in Änderungsliste

- key-value-interface for change commands
- error message if an unknown author id is used
- added space before author name in list of changes

### Fixed

- Fehler (Crash) in Änderungsliste gefixt, der bei Sonderzeichen auftrat

- fixed bug (crash) with special characters in list of changes


## [0.6.0] - 2012-01-11

### Added

- Italienische Übersetzungen der captions von Daniele Giovannini
- neues Nutzerinterface für das Setzen von Optionen sowie die Definition von Markup und Autoren
- Beispieldateien für alle Optionen und Befehle

- Italian translations of captions by Daniele Giovannini
- redefined user interface for setting options and definitions of markup and authors
- example files for all options and commands

### Changed

- Restrukturierung und Codeverbesserung
- verbesserte Dokumentation mit typischem Anwendungsfall
- Anmerkungen sind per Default nicht mehr farbig

- restructuring and code improvement
- improved documentation including typical use case
- by default remarks are not colored anymore


## [0.5.4] - 2011-04-25

### Added

- neues Script, um die `changes`-Befehle zu löschen von Silvano Chiaradonna

- new script for removal of `changes` commands by Silvano Chiaradonna

### Changed

- Auslagerung der Nutzerdokumentation in eigene Datei
- Änderung der default-Sprache zu Englisch

- extraction of user documentation in separate file
- default language changed to English


## [0.5.3] - 2010-11-22

### Added

- Dokumentoptionen von `documentclass` werden ebenfalls genutzt (Vorschlag und Code von Steve Wolter)

- document options of `documentclass` are used too (suggestion and code by Steve Wolter)


## [0.5.2] - 2007-10-10

### Added

- Paketoptionen der Pakete `ulem` und `xcolor` werden an diese weitergeleitet

- package options for `ulem` and `xcolor` are passed to the packages


## [0.5.1] - 2007-08-27

### Fixed

- gelöschter Text wieder durchgestrichen, Paket `ulem` funktioniert; ausgrauen hat nicht funktioniert

- deleted text is striked out again using package `ulem`, greying didn't work


## [0.5] - 2007-08-26

### Added

- neues optionales Argument für Autorenname
- farbige Liste der Änderungen

- new optional argument for author's name
- colored list of changes

### Changed

- auf UTF-8-encoding umgestellt
- gelöschter Text durch grauen Hintergrund visualisiert (es gibt bisher kein ordentliches Durchstreichen bei UTF-8-Nutzung)
- loc-Format geändert
- englische Doku verbessert

- switch to UTF-8-encoding
- markup for deleted text changed to gray background, because there's no possibility to conveniently strike out UTF-8-text
- changed loc file format
- improved English documentation

### Fixed

- keine Nutzung des `arrayjob`-Pakets mehr, dadurch Fehler im Zusammenspiel mit `array` behoben
- keine Nutzung des `soul`-Pakets mehr, dadurch Fehler im Zusammenspiel UTF-8-encoding behoben

- no usage of package `arrayjob` anymore, thus no errors using package `array`
- no usage of package `soul` anymore, thus no errors using UTF-8-encoding


## [0.4] - 2007-01-24

- erste Version für das CTAN

- first version uploaded to CTAN

### Changed

- `setremarkmarkup` um Autor-ID erweitert, um Anmerkung farbig setzen zu können
- Anmerkungen werden in der Fußnote farbig gesetzt

- extended `setremarkmarkup` with author's id for using color in remarks
- by default remarks are colored now

### Fixed

- `pdfcolmk` eingebunden, um Problem mit farbigem Text bei Seitenumbrüchen zu lösen

- included `pdfcolmk` to solve problem with colored text and page breaks


## [0.3] - 2007-01-22

### Added

- englische Nutzerdokumentation

- English user-documentation

### Changed

- Befehl `changed` ersetzt durch `replaced`

- replaced command `changed` with `replaced`

### Fixed

- verbesserte `final`-Option: kein zusätzlicher Leerraum

- improved `final`-option: no additional space


## [0.2] - 2007-01-17

### Added

- `setauthormarkup`, `setlocextension`, `setremarkmarkup` für Einstellungen
- Beispieldateien generiert
- LPPL eingefügt

- new commands `setauthormarkup`, `setlocextension`, `setremarkmarkup`
- generated examples
- inserted LPPL

### Fixed

- Bezeichnungen auch bei fehlendem `babel`-Paket eingeführt
- Fehler mit `ifthen`-Paketplazierung behoben
- bei Liste war immer "Eingefügt" eingestellt, behoben
- Autorausgabe war buggy (`if`-Abfrage nicht einwandfrei)

- defined loc-names when missing `babel`-package
- fixed wrong `ifthen` package placement
- fixed error in loc, always showing "added"
- fixed authormarkup (`if`-condition not bugfree)


## [0.1] - 2007-01-16

- initiale Version

- initial version

### Added

- Befehle `added`, `deleted` und `changed`

- commands `added`, `deleted`, and `changed`
